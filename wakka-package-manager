#!/usr/bin/env python2
# This code is part of Wakka.

# Wakka is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# Wakka is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Wakka; if not, write to the Free Software Foundation, 
# Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Wakka is based on code from gtkPacman, and is copyright (C)2009-2010
# Mitchell Nemitz. gtkPacman is copyright (C)2005-2008 Stefano Esposito.

def get_fname_and_icons():
    from os.path import exists, abspath, join

    path = "/usr/lib/wakka/"
    
    if not exists(path):
        path = abspath("data/")
    else:
        path = join(path, "data/")
    
    icons = {}
    fname = join(path, "wakka.glade")
    
    icons["red"]    = join(path, "icons/red_dot.png")
    icons["yellow"] = join(path, "icons/yellow_dot.png")
    icons["green"]  = join(path, "icons/green_dot.png")
    icons["pacman"] = join(path, "icons/pacman.svg")
    
    print "Data path: %s" %path
    
    return fname, icons


def make_icons(icons):
    from gtk.gdk import pixbuf_new_from_file
    from gtk     import IconSet, IconFactory
    
    for icon_name in icons.keys():
        icon         = pixbuf_new_from_file(icons[icon_name])
        icon_set     = IconSet(icon)
        icon_factory = IconFactory()
        
        icon_factory.add(icon_name, icon_set)
        icon_factory.add_default()
    
    return

def setup_sync(icon):
    from wakkacore import dialogs
    dlg = dialogs.setup_sync_dialog(icon)
    dlg.show_all()

if __name__ == "__main__":
    from os        import getuid, geteuid
    from os.path   import exists
    from locale    import setlocale, LC_ALL
    from gettext   import install
    from gtk       import main
    from gtk.glade import bindtextdomain, textdomain
    from wakkacore import gui, database
    
    fname, icons = get_fname_and_icons()
    make_icons(icons)
    setlocale(LC_ALL, '')
    
    if geteuid() == 0:
        setup_sync(icons["pacman"])

    if exists("/usr/share/locale/it/LC_MESSAGES/wakka.mo"):
        path="/usr/share/locale/"
    else:
        path="data/locale/"
        
    bindtextdomain('wakka', path)
    textdomain('wakka')
    install('wakka', path, unicode=1)
    database = database()
    gui = gui(fname, database, getuid(), icons["pacman"])
    
    print "Language support path: %s" %path
    print "Pacman version : %s" %database.ver
    
    main()
