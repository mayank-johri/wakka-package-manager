#!/bin/bash

echo "Installing Wakka..."

if [ -d "/usr/lib/wakka/" ] ; then
    echo "Install directory exitsts..."
else
    mkdir "/usr/lib/wakka/"
fi

if [ -d "/usr/local/bin/" ] ; then
    echo "Binary directory exitsts..."
else
    mkdir "/usr/bin/"
fi

echo "Copying core files..."

# Copy the core data files
cp --parents -R "wakkacore/" "/usr/lib/wakka/"
cp --parents -R "data/"      "/usr/lib/wakka/"

# Copy license and readme information
cp "COPYING" "/usr/lib/wakka/"
cp "LICENSE" "/usr/lib/wakka/"
cp "README"  "/usr/lib/wakka/"

# Copy over the executables
cp "wakka-package-manager" "/usr/lib/wakka/"
cp "wakka"                 "/usr/bin/"

# Make it executable
chmod +x "/usr/bin/wakka"

# Add the menu entry
cp "data/wakka.desktop" "/usr/share/applications/"

echo "Installation complete!"
